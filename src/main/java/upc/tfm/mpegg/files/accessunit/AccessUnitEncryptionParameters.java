package upc.tfm.mpegg.files.accessunit;


import lombok.Getter;
import lombok.Setter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import upc.tfm.mpegg.files.EncryptionParameter;
import upc.tfm.mpegg.files.utils.MPEGG_CIPHERS;

import javax.xml.parsers.ParserConfigurationException;

@Getter
@Setter
public class AccessUnitEncryptionParameters extends EncryptionParameter {

    private byte[] inIV;
    private byte[] inTAG;

    public AccessUnitEncryptionParameters(Document patherDocument){
        cipher = MPEGG_CIPHERS.AES_128_GCM;
        rootElement = patherDocument.createElement("AccessUnitEncryptionParameters");
    }

    public AccessUnitEncryptionParameters(Document patherDocument, MPEGG_CIPHERS cipher){
        this.cipher = cipher;
        rootElement = patherDocument.createElement("AccessUnitEncryptionParameters");
    }

    @Override
    public void generateValues() {
        int TAG_LENGTH;
        int IV_LENGTH;
        if(getOperationMode().compareTo("GCM") == 0){
            TAG_LENGTH = GCM_TAG_LENGTH;
            IV_LENGTH = GCM_IV_LENGTH;
        } else {
            TAG_LENGTH = GCM_TAG_LENGTH;
            IV_LENGTH = CTR_IV_LENGTH;
        }
        TAG = generateRandomByteArray(TAG_LENGTH);
        inTAG = generateRandomByteArray(TAG_LENGTH);
        IV = generateRandomByteArray(IV_LENGTH);
        inIV = generateRandomByteArray(TAG_LENGTH);
    }


    public Element writeEncryptionParameters()  {
        try {
            rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "cipher", cipher.value()));
            if(getOperationMode().compareTo("GCM") == 0)  rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "aublockTAG", encodeDataToBase64(TAG)));
            rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "auinIV", encodeDataToBase64(inIV)));
            rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "auinTAG", encodeDataToBase64(inTAG)));
            rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "aublockIV", encodeDataToBase64(IV)));
           return rootElement;

        } catch (Exception e) {
            throw e;
        }
    }
}
