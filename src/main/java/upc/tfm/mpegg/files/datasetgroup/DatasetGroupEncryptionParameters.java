package upc.tfm.mpegg.files.datasetgroup;


import lombok.Getter;
import lombok.Setter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import upc.tfm.mpegg.files.EncryptionParameter;
import upc.tfm.mpegg.files.utils.MPEGG_CIPHERS;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Base64;

@Getter
@Setter
public class DatasetGroupEncryptionParameters extends EncryptionParameter {

    private String configurationID;
    private String encryptedLocations;

    public DatasetGroupEncryptionParameters(Document patherDocument){
        cipher = MPEGG_CIPHERS.AES_128_GCM;
        rootElement = patherDocument.createElement("EncryptionParameters");
    }

    public DatasetGroupEncryptionParameters(Document patherDocument, MPEGG_CIPHERS cipher, String encryptedLocations, String configurationID){
        this.cipher = cipher;
        this.configurationID = configurationID;
        this.encryptedLocations = encryptedLocations;
        rootElement = patherDocument.createElement("EncryptionParameters");
    }


    public Element writeEncryptionParameters() throws ParserConfigurationException {

        try {
            if (encryptedLocations != null)  rootElement.setAttribute("encryptedLocations", encryptedLocations);
            if (configurationID != null)  rootElement.setAttribute("configurationID", configurationID);

            rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "cipher", cipher.value()));
            rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "IV", encodeDataToBase64(IV)));
            if(getOperationMode().compareTo("GCM") == 0)  rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "TAG", encodeDataToBase64(TAG)));

            return rootElement;

        } catch (Exception e) {
            throw e;
        }
    }
}
