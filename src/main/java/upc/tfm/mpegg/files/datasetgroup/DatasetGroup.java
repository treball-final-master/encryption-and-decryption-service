package upc.tfm.mpegg.files.datasetgroup;


import org.w3c.dom.Document;



import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;


import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;

import upc.tfm.mpegg.encryptionanddecryption.Domain.MPEGGProtectionBox;
import upc.tfm.mpegg.files.dataset.DatasetEncryptionParameters;
import upc.tfm.mpegg.files.utils.MPEGG_CIPHERS;

import org.xml.sax.SAXException;


public class DatasetGroup extends MPEGGProtectionBox {

    private final String URI = "mpg-access-prot:DatasetGroupProtectionType";
    private final String qualifiedName = "DatasetGroupProtection";


    public DatasetGroup(MPEGG_CIPHERS cipher, String encryptedLocations, String configurationID) throws ParserConfigurationException {
        createBaseDocument(URI, qualifiedName);
        encryptionParameter = new DatasetGroupEncryptionParameters(document, cipher, encryptedLocations, configurationID);
    }

    public DatasetGroup(String xmlStringFile) throws ParserConfigurationException {
        createBaseDocument(URI, qualifiedName);
        encryptionParameter = new DatasetGroupEncryptionParameters(document);
        try {
            Document doc = getXmlFromString(xmlStringFile);
            parseXmlData(doc);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void parseXmlData(Document doc) {
        byte[] IV = decodeBase64Value(extractXmlTagValue(doc, "IV"));
        encryptionParameter.setIV(IV);
        byte[] TAG = decodeBase64Value(extractXmlTagValue(doc, "TAG"));
        encryptionParameter.setTAG(TAG);

        String cipher = extractXmlTagValue(doc, "cipher");
        encryptionParameter.setCipher(MPEGG_CIPHERS.getFromURI(cipher));
    }

    @Override
    public String encrypt(String data, byte[] byteKey) throws InvalidAlgorithmParameterException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = generateCipher();
        Key key = new SecretKeySpec(byteKey, "AES");
        encryptionParameter.generateValues();
        AlgorithmParameterSpec algorithmParameterSpec = getParametersSpec();
        cipher.init(Cipher.ENCRYPT_MODE, key, algorithmParameterSpec );
        return encodeBase64(cipher.doFinal(data.getBytes(StandardCharsets.UTF_8)));
    }



    @Override
    public String decrypt(String data, byte[] byteKey) throws InvalidAlgorithmParameterException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = generateCipher();

        Key key = new SecretKeySpec(byteKey, "AES");;
        AlgorithmParameterSpec algorithmParameterSpec = getParametersSpec();

        byte [] byteData = decodeBase64Value(data);
        cipher.init(Cipher.DECRYPT_MODE, key, algorithmParameterSpec);

        return new String(cipher.doFinal(byteData));
    }
}

