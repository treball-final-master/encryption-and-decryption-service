package upc.tfm.mpegg.files.dataset;

import lombok.Getter;
import lombok.Setter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import upc.tfm.mpegg.files.EncryptionParameter;
import upc.tfm.mpegg.files.utils.MPEGG_CIPHERS;



import javax.xml.parsers.ParserConfigurationException;

@Getter
@Setter
public class DatasetEncryptionParameters extends EncryptionParameter {

    private String encryptedLocations;
    private String configurationID;

    public DatasetEncryptionParameters(Document patherDocument){
        cipher = MPEGG_CIPHERS.AES_128_GCM;
        rootElement = patherDocument.createElement("EncryptionParameters");
    }

    public DatasetEncryptionParameters(Document patherDocument, MPEGG_CIPHERS cipher, String encryptedLocations, String configurationID){
        this.cipher = cipher;
        this.configurationID = configurationID;
        this.encryptedLocations = encryptedLocations;
        rootElement = patherDocument.createElement("EncryptionParameters");
    }


    public Element writeEncryptionParameters() throws ParserConfigurationException {

        try {
            if (encryptedLocations != null)  rootElement.setAttribute("encryptedLocations", encryptedLocations);
            if (configurationID != null)  rootElement.setAttribute("configurationID", configurationID);

            rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "cipher", cipher.value()));
            rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "IV", encodeDataToBase64(IV)));
            rootElement.appendChild(getNode(rootElement.getOwnerDocument(), "TAG", encodeDataToBase64(TAG)));
            return rootElement;

        } catch (Exception e) {
            throw e;
        }
    }



}
