package upc.tfm.mpegg.files;

import lombok.Getter;
import lombok.Setter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import upc.tfm.mpegg.files.utils.MPEGG_CIPHERS;

import javax.xml.parsers.ParserConfigurationException;
import java.security.SecureRandom;
import java.util.Base64;

@Getter
@Setter
public abstract class EncryptionParameter {

    protected byte[] IV;
    protected byte[] TAG;
    protected MPEGG_CIPHERS cipher;
    protected Element rootElement;

    public static final int GCM_IV_LENGTH = 12;
    public static final int GCM_TAG_LENGTH = 16;
    public static final int CTR_IV_LENGTH = 16;
    public void generateValues() {
        int TAG_LENGTH;
        int IV_LENGTH;
        if(getOperationMode().compareTo("GCM") == 0){
            TAG_LENGTH = GCM_TAG_LENGTH;
            IV_LENGTH = GCM_IV_LENGTH;
        } else {
            TAG_LENGTH = GCM_TAG_LENGTH;
            IV_LENGTH = CTR_IV_LENGTH;
        }
        TAG = generateRandomByteArray(TAG_LENGTH);
        IV = generateRandomByteArray(IV_LENGTH);
    }

    protected byte[] generateRandomByteArray(int size) {
        SecureRandom random = new SecureRandom();
        byte[] byteArray = new byte[size];
        random.nextBytes(byteArray);
        return byteArray.clone();
    }

    protected Node getNode(Document doc, String tagName, String value) {
        Element XMLelement = doc.createElement(tagName);
        XMLelement.setTextContent(value);
        return XMLelement;
    }

    abstract  public Element writeEncryptionParameters() throws ParserConfigurationException;

    protected String encodeDataToBase64( byte [] data){
        return Base64.getMimeEncoder().encodeToString(data);
    }

    public String getOperationMode() {
        switch (cipher) {
            case AES_256_CTR:
            case AES_192_CTR:
            case AES_128_CTR:
                return "CTR";
            case AES_192_GCM:
            case AES_128_GCM:
            case AES_256_GCM:
                return "GCM";
        }
        return "CHACHA";
    }
}
