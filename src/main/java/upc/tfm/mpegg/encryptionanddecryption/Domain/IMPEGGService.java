package upc.tfm.mpegg.encryptionanddecryption.Domain;

public interface IMPEGGService {
    String encryptAccessUnit();
    String encryptDataset();
    String encryptDatasetGroup();
    String decryptAccessUnit();
    String decryptDataset();
    String decryptDatasetGroup();
}
