package upc.tfm.mpegg.encryptionanddecryption.controller.Responses;

import lombok.Getter;
import lombok.Setter;
import upc.tfm.mpegg.encryptionanddecryption.Domain.MPEGG;
import upc.tfm.mpegg.encryptionanddecryption.controller.Request.RequestDecryption;
import upc.tfm.mpegg.files.dataset.Dataset;
import upc.tfm.mpegg.files.datasetgroup.DatasetGroup;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.parsers.ParserConfigurationException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Getter
@Setter
public class PostDatasetGroupDecryptionResponse {

    private String decryptedData;

    public PostDatasetGroupDecryptionResponse(RequestDecryption requestDatasetGroupDecryption) throws ParserConfigurationException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        MPEGG mpegg = new MPEGG();
        DatasetGroup dt = new DatasetGroup(
                requestDatasetGroupDecryption.getProtectionBox()
        );
        mpegg.setDatasetGroup(dt);

        byte[] key = Base64.getMimeDecoder().decode(requestDatasetGroupDecryption.getKey());
        decryptedData = mpegg.decryptDatasetGroup(requestDatasetGroupDecryption.getEncryptedData(), key);
    }
}
