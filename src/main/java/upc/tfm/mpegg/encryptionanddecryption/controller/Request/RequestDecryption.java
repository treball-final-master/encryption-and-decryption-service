package upc.tfm.mpegg.encryptionanddecryption.controller.Request;


import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.ResponseBody;
@Getter
@Setter
@ResponseBody
public class RequestDecryption {
    private String encryptedData;
    private String key;
    private String protectionBox;
}
