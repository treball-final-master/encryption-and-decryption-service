package upc.tfm.mpegg.encryptionanddecryption.controller.Responses;

import lombok.Getter;
import lombok.Setter;
import upc.tfm.mpegg.encryptionanddecryption.Domain.MPEGG;
import upc.tfm.mpegg.encryptionanddecryption.controller.Request.RequestDecryption;
import upc.tfm.mpegg.files.accessunit.AccessUnit;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.parsers.ParserConfigurationException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Getter
@Setter
public class PostDecryptionResponse {

    private String decryptedData;

    public PostDecryptionResponse(RequestDecryption requestDecryption) throws ParserConfigurationException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        MPEGG mpegg = new MPEGG();
        AccessUnit au = new AccessUnit(
                requestDecryption.getProtectionBox()
        );
        mpegg.setAccessUnit(au);
        byte[] key = Base64.getMimeDecoder().decode(requestDecryption.getKey());
        decryptedData = mpegg.decryptAccessUnit(requestDecryption.getEncryptedData(), key);
    }
}
