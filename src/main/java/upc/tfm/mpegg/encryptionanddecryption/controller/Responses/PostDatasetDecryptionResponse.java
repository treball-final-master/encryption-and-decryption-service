package upc.tfm.mpegg.encryptionanddecryption.controller.Responses;

import lombok.Getter;
import lombok.Setter;
import upc.tfm.mpegg.encryptionanddecryption.Domain.MPEGG;
import upc.tfm.mpegg.encryptionanddecryption.controller.Request.RequestDecryption;
import upc.tfm.mpegg.files.accessunit.AccessUnit;
import upc.tfm.mpegg.files.dataset.Dataset;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.parsers.ParserConfigurationException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Getter
@Setter
public class PostDatasetDecryptionResponse {

    private String decryptedData;

    public PostDatasetDecryptionResponse(RequestDecryption requestDecryption) throws ParserConfigurationException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        MPEGG mpegg = new MPEGG();
        Dataset dt = new Dataset(
                requestDecryption.getProtectionBox()
        );
        mpegg.setDataset(dt);

        byte[] key = Base64.getMimeDecoder().decode(requestDecryption.getKey());
        decryptedData = mpegg.decryptDataset(requestDecryption.getEncryptedData(), key);
    }
}
