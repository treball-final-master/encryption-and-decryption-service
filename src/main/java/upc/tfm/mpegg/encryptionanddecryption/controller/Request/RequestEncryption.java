package upc.tfm.mpegg.encryptionanddecryption.controller.Request;


import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.ResponseBody;
import upc.tfm.mpegg.files.utils.MPEGG_CIPHERS;



@Getter
@Setter
@ResponseBody
public class RequestEncryption {
    private MPEGG_CIPHERS cipher = MPEGG_CIPHERS.AES_128_CTR;
    private String key;
    private String data;
    private String configurationId = null;
    private String encryptedLocations = null;
}
