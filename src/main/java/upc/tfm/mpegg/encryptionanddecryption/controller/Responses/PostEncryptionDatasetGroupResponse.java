package upc.tfm.mpegg.encryptionanddecryption.controller.Responses;

import lombok.Getter;
import lombok.Setter;
import org.xml.sax.SAXException;
import upc.tfm.mpegg.encryptionanddecryption.Domain.MPEGG;
import upc.tfm.mpegg.encryptionanddecryption.controller.Request.RequestEncryption;
import upc.tfm.mpegg.files.dataset.Dataset;
import upc.tfm.mpegg.files.datasetgroup.DatasetGroup;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Base64;


@Getter
@Setter
public class PostEncryptionDatasetGroupResponse {

    private String encryptedData;
    private String protectionBox;
    private String key;

    public PostEncryptionDatasetGroupResponse(RequestEncryption requestEncryption) throws ParserConfigurationException, MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        MPEGG mpegg = new MPEGG();
        DatasetGroup dt = new DatasetGroup(requestEncryption.getCipher(),requestEncryption.getEncryptedLocations(), requestEncryption.getConfigurationId());
        mpegg.setDatasetGroup(dt);
        mpegg.generateKey();
        System.out.println("da");
        System.out.println(requestEncryption.getData());
        encryptedData = mpegg.encryptDatasetGroup(requestEncryption.getData());

        protectionBox = mpegg.signDatasetGroup();
        key = Base64.getMimeEncoder().encodeToString(mpegg.getKey());

    }


}
