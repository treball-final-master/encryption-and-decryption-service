package upc.tfm.mpegg.encryptionanddecryption.application;

public interface EncryptionService {
    void encryptFile();
}
