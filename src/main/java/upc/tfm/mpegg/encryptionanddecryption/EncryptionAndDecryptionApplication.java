package upc.tfm.mpegg.encryptionanddecryption;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.xml.sax.SAXException;
import upc.tfm.mpegg.encryptionanddecryption.Domain.MPEGG;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

@SpringBootApplication
public class EncryptionAndDecryptionApplication {

    public static void main(String[] args) throws ParserConfigurationException, MarshalException, InvalidAlgorithmParameterException, UnrecoverableEntryException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, XMLSignatureException, TransformerException, SAXException {
        SpringApplication.run(EncryptionAndDecryptionApplication.class, args);
    }

}
