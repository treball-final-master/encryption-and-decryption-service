PORT=8081

build-project:
	./mvnw package -Dmaven.test.skip=true

run-project: build-project
	java -jar target/encryption-and-decryption-0.0.1-SNAPSHOT.jar

docker-build: build-project
	docker build -t registry.gitlab.com/treball-final-master/encryption-and-decryption-service .

docker-push: docker-build
	docker push registry.gitlab.com/treball-final-master/encryption-and-decryption-service

docker-run: docker-build
	docker run -p ${PORT}:8080 --rm --name encrypt-decrypt-service registry.gitlab.com/treball-final-master/encryption-and-decryption-service

2 definicions XACML
maca